package ua.epam.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.epam.training.exception.NoSuchAuthorException;
import ua.epam.training.exception.NoSuchBookException;
import ua.epam.training.model.Book;
import ua.epam.training.model.dto.BookDTO;
import ua.epam.training.service.BookService;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(value = "/api/book/{book_id}")
    public ResponseEntity<BookDTO> getBook(@PathVariable Long book_id) throws NoSuchBookException, NoSuchAuthorException {
        Book book = bookService.getBook(book_id);
        Link link = linkTo(methodOn(BookController.class).getBook(book_id)).withSelfRel();

        BookDTO bookDTO = new BookDTO(book, link);

        return new ResponseEntity<>(bookDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/api/book/author/{author_id}")
    public ResponseEntity<List<BookDTO>> getBooksByAuthorId(@PathVariable Long author_id) throws NoSuchAuthorException {
        List<Book> bookList = bookService.getBooksByAuthorId(author_id);

        Link link = linkTo(methodOn(BookController.class).getAllBooks()).withSelfRel();

        List<BookDTO> booksDTO = new ArrayList<>();
        for (Book entity : bookList) {
            Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
            BookDTO dto = new BookDTO(entity, selfLink);
            booksDTO.add(dto);
        }

        return new ResponseEntity<>(booksDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/api/book")
    public ResponseEntity<List<BookDTO>> getAllBooks() throws NoSuchAuthorException {
        List<Book> bookList = bookService.getAllBooks();
        Link link = linkTo(methodOn(BookController.class).getAllBooks()).withSelfRel();

        List<BookDTO> booksDTO = new ArrayList<>();
        for (Book entity : bookList) {
            Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
            BookDTO dto = new BookDTO(entity, selfLink);
            booksDTO.add(dto);
        }
        return new ResponseEntity<>(booksDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/api/book")
    public ResponseEntity<BookDTO> addBook(@RequestBody Book newBook) throws NoSuchBookException, NoSuchAuthorException {
        bookService.createBook(newBook);
        Link link = linkTo(methodOn(BookController.class).getBook(newBook.getId())).withSelfRel();
        BookDTO bookDTO = new BookDTO(newBook, link);

        return new ResponseEntity<>(bookDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/api/book/{book_id}")
    public ResponseEntity<BookDTO> updateBook(@RequestBody Book newBook, @PathVariable Long book_id) throws NoSuchBookException, NoSuchAuthorException {
        bookService.updateBook(newBook, book_id);
        Book book = bookService.getBook(book_id);
        Link link = linkTo(methodOn(BookController.class).getBook(book_id)).withSelfRel();
        BookDTO bookDTO = new BookDTO(book, link);

        return new ResponseEntity<>(bookDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/book/{book_id}")
    public ResponseEntity deleteBook(@PathVariable Long book_id) throws NoSuchBookException {
        bookService.deleteBook(book_id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
