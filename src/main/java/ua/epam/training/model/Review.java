package ua.epam.training.model;

import javax.persistence.*;

@Entity
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "stars_num")
    private Integer starsNum;

    private String comment;

    @ManyToOne
    private Book book;

    public Review() {
    }

    public Review(Integer starsNum, String comment, Book book) {
        this.starsNum = starsNum;
        this.comment = comment;
        this.book = book;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStarsNum() {
        return starsNum;
    }

    public void setStarsNum(Integer starsNum) {
        this.starsNum = starsNum;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", starsNum=" + starsNum +
                ", comment='" + comment + '\'' +
                '}';
    }
}
