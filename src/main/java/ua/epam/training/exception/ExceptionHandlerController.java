package ua.epam.training.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(NoSuchBookException.class)
    public ResponseEntity handleNoSuchBookException() {
        return new ResponseEntity<>("Book not found", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NoSuchAuthorException.class)
    public ResponseEntity handleNoSuchAuthorException() {
        return new ResponseEntity<>("Author not found", HttpStatus.NOT_FOUND);
    }


}
