package ua.epam.training.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.epam.training.exception.NoSuchAuthorException;
import ua.epam.training.model.Author;
import ua.epam.training.model.Book;
import ua.epam.training.repository.AuthorRepository;
import ua.epam.training.repository.BookRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AuthorService {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;

    @Autowired
    public AuthorService(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    public Author getAuthor(Long authorId) throws NoSuchAuthorException {
        return authorRepository.findById(authorId).orElseThrow(NoSuchAuthorException::new);
    }

    public List<Author> getAuthorsByBookId(Long bookId) throws NoSuchAuthorException {
        Book book = bookRepository.findById(bookId).orElseThrow(NoSuchAuthorException::new);
        return book.getAuthors();
    }

    public List<Author> getAuthors() {
        return authorRepository.findAll();
    }

    @Transactional
    public void createAuthor(Author author) {
        authorRepository.save(author);
    }

    @Transactional
    public void updateAuthor(Author newAuthor, Long authorId) throws NoSuchAuthorException {
        Author author = authorRepository.findById(authorId).orElseThrow(NoSuchAuthorException::new);

        author.setName(newAuthor.getName());
        author.setBooks(newAuthor.getBooks());
        authorRepository.save(author);
    }

    @Transactional
    public void deleteAuthor(Long authorId) throws NoSuchAuthorException {
        if (authorRepository.existsById(authorId)) {
            authorRepository.deleteById(authorId);
        } else {
            throw new NoSuchAuthorException();
        }
    }
}
