package ua.epam.training.service;

import ua.epam.training.exception.NoSuchAuthorException;
import ua.epam.training.exception.NoSuchBookException;
import ua.epam.training.model.Author;
import ua.epam.training.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.epam.training.repository.AuthorRepository;
import ua.epam.training.repository.BookRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class BookService {

    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    @Autowired
    public BookService(BookRepository bookRepository, AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }

    public Book getBook(Long bookId) throws NoSuchBookException {
        return bookRepository.findById(bookId).orElseThrow(NoSuchBookException::new);
    }

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public List<Book> getBooksByAuthorId(Long authorId) throws NoSuchAuthorException {
        Author author = authorRepository.findById(authorId).orElseThrow(NoSuchAuthorException::new);
        return author.getBooks();
    }

    @Transactional
    public void createBook(Book book) {
        bookRepository.save(book);
    }

    @Transactional
    public void updateBook(Book newBook, Long bookId) throws NoSuchBookException {
        Book book = bookRepository.findById(bookId).orElseThrow(NoSuchBookException::new);

        book.setTitle(newBook.getTitle());
        book.setDescription(newBook.getDescription());
        book.setImageUrl(newBook.getImageUrl());
        book.setReleaseDate(newBook.getReleaseDate());
        book.setAuthors(newBook.getAuthors());
        book.setReviews(newBook.getReviews());
        bookRepository.save(book);
    }

    @Transactional
    public void deleteBook(Long bookId) throws NoSuchBookException {
        if (bookRepository.existsById(bookId)) {
            bookRepository.deleteById(bookId);
        } else {
            throw new NoSuchBookException();
        }
    }
}
