package ua.epam.training.model.dto;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import ua.epam.training.controller.BookController;
import ua.epam.training.exception.NoSuchAuthorException;
import ua.epam.training.model.Author;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class AuthorDTO extends ResourceSupport {

    private Author author;

    public AuthorDTO(Author author, Link selfLink) throws NoSuchAuthorException {
        this.author = author;
        add(selfLink);
        add(linkTo(methodOn(BookController.class).getBooksByAuthorId(author.getId())).withRel("books"));
    }

    public Long getAuthorId() {
        return author.getId();
    }

    public String getName() {
        return author.getName();
    }
}
