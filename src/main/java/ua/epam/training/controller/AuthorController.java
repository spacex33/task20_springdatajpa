package ua.epam.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.epam.training.exception.NoSuchAuthorException;
import ua.epam.training.model.Author;
import ua.epam.training.model.dto.AuthorDTO;
import ua.epam.training.service.AuthorService;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class AuthorController {

    private AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping(value = "/api/author/{author_id}")
    public ResponseEntity<AuthorDTO> getAuthor(@PathVariable Long author_id) throws NoSuchAuthorException {
        Author author = authorService.getAuthor(author_id);
        Link link = linkTo(methodOn(AuthorController.class).getAuthor(author_id)).withSelfRel();

        AuthorDTO authorDTO = new AuthorDTO(author, link);

        return new ResponseEntity<>(authorDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/api/author/book/{book_id}")
    public ResponseEntity<List<AuthorDTO>> getAuthorsByBookId(@PathVariable Long book_id) throws NoSuchAuthorException {
        List<Author> authorList = authorService.getAuthorsByBookId(book_id);

        Link link = linkTo(methodOn(AuthorController.class).getAllAuthors()).withSelfRel();

        List<AuthorDTO> authorsDTO = new ArrayList<>();
        for (Author entity : authorList) {
            Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
            AuthorDTO dto = new AuthorDTO(entity, selfLink);
            authorsDTO.add(dto);
        }

        return new ResponseEntity<>(authorsDTO, HttpStatus.OK);
    }


    @GetMapping(value = "/api/author")
    public ResponseEntity<List<AuthorDTO>> getAllAuthors() throws NoSuchAuthorException {
        List<Author> authorList = authorService.getAuthors();
        Link link = linkTo(methodOn(AuthorController.class).getAllAuthors()).withSelfRel();

        List<AuthorDTO> authorsDTO = new ArrayList<>();
        for (Author entity : authorList) {
            Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
            AuthorDTO dto = new AuthorDTO(entity, selfLink);
            authorsDTO.add(dto);
        }

        return new ResponseEntity<>(authorsDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/api/author")
    public ResponseEntity<AuthorDTO> addAuthor(@RequestBody Author newAuthor) throws NoSuchAuthorException {
        authorService.createAuthor(newAuthor);
        Link link = linkTo(methodOn(AuthorController.class).getAuthor(newAuthor.getId())).withSelfRel();

        AuthorDTO authorDTO = new AuthorDTO(newAuthor, link);

        return new ResponseEntity<>(authorDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/api/author/{author_id}")
    public ResponseEntity<AuthorDTO> updateAuthor(@RequestBody Author newAuthor, @PathVariable Long author_id) throws NoSuchAuthorException {
        authorService.updateAuthor(newAuthor, author_id);
        Author author = authorService.getAuthor(author_id);
        Link link = linkTo(methodOn(AuthorController.class).getAuthor(author_id)).withSelfRel();

        AuthorDTO authorDTO = new AuthorDTO(author, link);

        return new ResponseEntity<>(authorDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/author/{author_id}")
    public ResponseEntity deleteAuthor(@PathVariable Long author_id) throws NoSuchAuthorException {
        authorService.deleteAuthor(author_id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
