package ua.epam.training.model.dto;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import ua.epam.training.controller.AuthorController;
import ua.epam.training.exception.NoSuchAuthorException;
import ua.epam.training.model.Book;

import java.util.Date;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class BookDTO extends ResourceSupport{

    private Book book;

    public BookDTO(Book book, Link selfLink) throws NoSuchAuthorException {
        this.book=book;
        add(selfLink);
        add(linkTo(methodOn(AuthorController.class).getAuthorsByBookId(book.getId())).withRel("authors"));
    }

    public Long getBookId() {
        return book.getId();
    }

    public String getTitle() {
        return book.getTitle();
    }

    public String getDescription() {
        return book.getDescription();
    }

    public String getImageUrl() {
        return book.getImageUrl();
    }

    public Date getReleaseDate() {
        return book.getReleaseDate();
    }
}
